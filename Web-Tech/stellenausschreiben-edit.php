<?php

if (!empty($_COOKIE['user'])) {
  $logged_user = $_COOKIE['user'];
}

//Database Connection
$connection = mysqli_connect('localhost', 'root', '', 'graduatemarket')
or die ("Connection Error");


?>
<html><head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
  <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="css\stylesheet.css" rel="stylesheet" type="text/css">
  <link href="css\jquery.flexdatalist.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="js/jquery.flexdatalist.js"></script>
  <link rel="icon" type="image/ico" href="favicon.ico">

</head><body>
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="index.php" class="navbar-brand"><img height="20" alt="Brand" src="images\logo_green_LightBG.png"></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        <li class="underline">
          <a href="stellenangebote.php">JOB suchen</a>
        </li>
        <li class="underline">
          <a href="unternehmen.php">Unternehmen finden</a>
        </li>
        <li>
          <a href="logout.php" class="underline" role = "button" aria-haspopup = "true" aria-expanded = "true">Ausloggen</a>
        </li>
      </ul>
    </div>
    <!--/.nav-collapse -->
  </div>
</nav>
<form method="post" action="upload_stellenausschreiben.php">
  <section class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section">
              <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <img src="C:\Users\Maximilian\Documents\graduatemarket\Web-Tech\images\placeimg_640_480_arch1.jpg" class="img-responsive">
                  </div>
                  <div class="col-md-6">

                      <div class="form-group">
                        <label class="control-label" for="exampleInputJobTitle">Job-Titel</label>
                        <input class="form-control" id="exampleInputJobTitle" placeholder="Geben Sie den Job-Titel ein" type="text" name="job_title">
                      </div>

                    <h3><input type="text" name="title" placeholder="Überschrift "></h3>

                      <div class="form-group">
                        <label class="control-label" for="exampleInputPerspektive">Beschreibung</label>
                        <textarea class="form-control" id="exampleInputPerspektive" placeholder="Geben Sie eine Überschrift ein" type="text" rows="4" cols="50" name="description"></textarea>
                      </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="section text-center">

      <div class="container">
        <div class="row">
          <div class="col-md-4 text-left" id="dreier-box-eins">
            <i class="fa fa-3x fa-fw text-success icon-dreier-box fa-institution"></i>
            <h2>Das Unternehmen</h2>
            <div class="form-group">
              <textarea class="form-control" id="exampleInputPerspektive" placeholder="Über das Unternehmen" type="text" name="about_company"></textarea>
            </div>
          </div>
          <div class="col-md-4 text-left" id="dreier-box-zwei">
            <i class="fa fa-3x fa-fw text-success icon-dreier-box fa-cogs"></i>
            <h2>Ihre Aufgaben</h2>
            <div class="form-group">
              <textarea class="form-control" id="exampleInputPerspektive" placeholder="Die Aufgaben" type="text" name="tasks"></textarea>
            </div>
          </div>
          <div class="col-md-4 text-left" id="dreier-box-drei">
            <i class="fa fa-3x fa-fw board-o text-success icon-dreier-box fa-graduation-cap"></i>
            <h2>Ihre Qualifikationen</h2>
            <div class="form-group">
              <textarea class="form-control" id="exampleInputPerspektive" placeholder="Die notwendigen Qualifikationen" type="text" name="qualifications"></textarea>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <button class="btn btn-primary" type="submit">Abschicken</button>
          </div>
        </div>
      </div>
    </section>
</form>
    <section class="modal fade" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Registrierung</h4>
          </div>
          <div class="modal-body">
            <form role="form" method="post" action="./register.php?page=2">
              <div class="form-group">
                <label class="control-label" for="exampleInputEmail1">Vorname</label>
                <input class="form-control" name="firstname" placeholder="Vorname" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="exampleInputPassword1">Nachname</label>
                <input class="form-control" name="lastname" placeholder="Nachname" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="exampleInputEmail1">E-Mail-Address</label>
                <input class="form-control" name="email" id="exampleInputEmail1" placeholder="E-Mail-Adresse" type="email" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="exampleInputPassword1">Paswort</label>
                <input class="form-control" name="password" id="password" placeholder="Passwort" type="password" >
              </div>
              <div class="form-group">
                <label class="control-label" for="exampleInputPassword1">Passwort wiederholen</label>
                <input class="form-control" name="password_confirm" id="password_confirm" placeholder="Passwort wiederholen" type="password"  onkeyup="checkPass(); return false;">
              </div>
              <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-primary active">
                  <input type="radio" name="options_graduate" id="option1" autocomplete="off"> Sind Sie Absolvent?</label>
                <label class="btn btn-primary">
                  <input type="radio" name="options_company" id="option2" autocomplete="off">Repräsentieren Sie ein Unternehmen?</label>
              </div>
              <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal">Schließen</a>
                <button type="submit" name="submit" class="btn btn-success">Los geht's!</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <footer class="section">
      <div class="container">
        <div class="row">
          <div class="col-sm-3 text-center">
            <img src="images\logo_green_LightBG.png" id="footer-logo" class="img-responsive">
            <div class="row">
              <div class="col-md-12 text-center">
                <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor
                  incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                  nostrud</p>
              </div>
            </div>
          </div>
          <div class="col-sm-2 text-center">
            <p class="text-info text-right"></p>
            <a href="impressum.php"><h3>Informationen</h3></a>
            <div class="row">
              <div class="col-md-12 text-center">
                <p class="text-center">Über uns
                  <br>Presse
                  <br>Arbeitgeber
                  <br>Karriere
                  <br>Kontakt</p>
              </div>
            </div>
          </div>
          <div class="col-sm-2">
            <p class="text-info text-right"></p>
            <a href="impressum.php"><h3 class="text-center">Webseite</h3></a>
            <div class="row">
              <div class="col-md-12 text-center">
                <p class="text-center">Sitemap
                  <br>Mobil
                  <br>Datenschutzerklärung
                  <br>Nutzungsbedingungen
                  <br>AGB</p>
              </div>
            </div>
          </div>
          <div class="col-sm-2">
            <p class="text-info text-right"></p>
            <a href="impressum.php"><h3 class="text-center">Unternehmen</h3></a>
            <div class="row">
              <div class="col-md-12 text-center">
                <p class="text-center">Team
                  <br>Location
                  <br>Karte
                  <br>History</p>
              </div>
            </div>
          </div>
          <div class="col-sm-2">
            <p class="text-info text-right"></p>
            <h3 class="text-center">Social</h3>
            <p></p>
            <div class="row">
              <div class="col-md-12 text-center">
                <a href="#"><i class="fa fa-3x fa-fw fa-instagram"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-twitter"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-facebook"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-github"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  

</body></html>