<?php
if (!empty($_COOKIE['user'])) {
  $logged_user = $_COOKIE['user'];

  $id = $_COOKIE['id'];
}
?>
<html><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css\stylesheet.css" rel="stylesheet" type="text/css">
    <link href="css\jquery.flexdatalist.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="js/jquery.flexdatalist.js"></script>
    <script type="text/javascript" src="js/checkPassword.js"></script>
	<link rel="icon" type="image/ico" href="favicon.ico">
  
  </head><body>
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="#" class="navbar-brand"><img height="20" alt="Brand" src="images\logo_green_LightBG.png"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="underline">
              <a href="stellenangebote.php">JOB suchen</a>
            </li>
            <li class="underline">
              <a href="unternehmen.php">Unternehmen finden</a>
            </li>

            <!--Checking if logged in. When yes display name else login form.-->
            <?php
            // Checking if you're logged in. When not you will have a nav login form to login or register.
              if (empty($_COOKIE['user'])){
              ?>
            <li class="dropdown" >
              <a href = "#" class="dropdown-toggle underline" data-toggle = "dropdown" role = "button" aria-haspopup = "true" aria-expanded = "true" > Einloggen <span class="caret" ></span ></a >
              <ul class="dropdown-menu" >
                <li >
                  <a data-toggle = "modal" data-target = "#myModal" class="btn" > Registrieren</a >
                </li >
                <li role = "separator" class="divider" ></li >
                <li class="dropdown-header" > Anmelden</li >
                <li>
                  <div class="col-md-12" >
                    <form role = "form" method = "post" action = "login.php?page=log" >
                      <div class="form-group" >
                        <label class="control-label" for="exampleInputEmail1" > Email - Adresse</label >
                        <input class="form-control" id = "exampleInputEmail1" placeholder = "Enter email" type = "email" name = "email" >
                      </div >
                      <div class="form-group" >
                        <label class="control-label" for="exampleInputPassword1" > Passwort</label >
                        <input class="form-control" id = "exampleInputPassword1" placeholder = "Password" type = "password" name = "password" >
                      </div >
                      <button type = "submit" class="btn btn-default" > Anmelden!</button >
                    </form >
                  </div >
                </li >
              </ul >
            </li >
            <?php }
            //When you're logged in. The Login Form changes to a nav 'Your Profile' and a Logout Button gets added.
                else {
            ?>
                    <li>
                      <a href = "profile-site.php" class="underline" role = "button" aria-haspopup = "true" aria-expanded = "true" > Your Profile </a >
                    </li>
                    <li>
                      <a href="logout.php" class="underline" role = "button" aria-haspopup = "true" aria-expanded = "true">Ausloggen</a>
                    </li>

            <?php
            }
            ?>
          </ul>
        </div>
        <!--/.nav-collapse -->
      </div>
    </nav>
    <section class="cover">
      <div class="background-image-fixed cover-image" style="background-image : url('images/with_overlay.jpg')"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h1 class="text-inverse">Arbeitslos nach dem Studium?
              <br>
              <b>Nicht mehr!</b>
            </h1>
            <p class="text-inverse">Perspektivlosigkeit? Keine Ahnung wohin die berufliche Reise nach dem Studium gehen soll? Informiere dich noch heute
			über atraktiven Stellenangebote!</p>
            <br>
            <br>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="input-group input-group-lg">
              <span class="input-group-btn">
                <button class="btn btn-default" id="go-button" type="button">Go!</button>
              </span>
              <script type="text/javascript">
                $('#go-button').click(function(){
                 checkInput($("#input-search-index").val());
                });

                function checkInput(searchQuery)
                {
                  if(searchQuery=="Arbeit")
                  {
                    window.location = "stellenangebote.php";
                  }
                  else if(searchQuery == "Job")
                  {
                    window.location = "stellenangebote.php";
                  }
                  else if(searchQuery == "Stellenausschreiben")
                  {
                    window.location = "stellenangebote.php";
                  }
                  else if(searchQuery == "Unternehmen")
                  {
                    window.location = "unternehmen.php";
                  }
                  else if(searchQuery == "Profil")
                  {
                    window.location = "login.php";
                  }
                  else if(searchQuery == "Stellenangebote")
                  {
                    window.location = "stellenangebote.php";
                  }
                  else
                  {
                    document.getElementById("search").submit();
                  }
                }
              </script>

              <input type="text" id="input-search-index" class="form-control flexdatalist" data-min-length="1" name="search-index"
              list="search-index" placeholder="Ich suche nach...." onkeydown="if (event.keyCode == 13) { checkInput(this.value); return false; }">
              <datalist id="search-index">
                <option value="Arbeit">Arbeit</option>
                <option value="Job">Job</option>
                <option value="Stellenaussschreiben">Stellenausschreiben</option>
                <option value="Unternehmen">Unternehmen</option>
                <option value="Profil">Profil</option>
                <option value="Stellenangebote">Stellenangebote</option>
              </datalist>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="section text-center">
      <div class="container">
        <div class="row">
          <div class="col-md-4 text-left" id="dreier-box-eins">
            <i class="fa fa-3x fa-child fa-fw text-success icon-dreier-box"></i>
            <h2>Feature 1</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisici elit,
              <br>sed eiusmod tempor incidunt ut labore et dolore magna aliqua.
              <br>Ut enim ad minim veniam, quis nostrud</p>
          </div>
          <div id="dreier-box-zwei" class="col-md-4 text-left">
            <i class="fa fa-3x fa-angellist fa-fw text-success icon-dreier-box"></i>
            <h2>Feature 2</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisici elit,
              <br>sed eiusmod tempor incidunt ut labore et dolore magna aliqua.
              <br>Ut enim ad minim veniam, quis nostrud</p>
          </div>
          <div id="dreier-box-drei" class="col-md-4 text-left">
            <i class="fa fa-3x fa-fw fa-keyboard-o text-success icon-dreier-box"></i>
            <h2>Feature 3</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisici elit,
              <br>sed eiusmod tempor incidunt ut labore et dolore magna aliqua.
              <br>Ut enim ad minim veniam, quis nostrud</p>
          </div>
        </div>
      </div>
    </section>
    <section class="section" id="proud-block">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <h1 class="grey-font">We're proud to be #1 on the Market</h1>
            <p class="grey-font">Von Studenten für Studenten. Mit einem Portal, was sich 
			  spzeiell an Hochschulabsolventen richtet, haben sich die ehemaligen Studenten
			  Tolga und Maxi ihren Traum verwirklicht. Sie standen ebenfalls vor Jobsuche und wissen,
			  wie kompliziert sich diese Suche gestalten kann.
              dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies
              nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
              Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In
              enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum
              felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus
              elementum semper nisi.</p>
          </div>
          <div class="col-md-4 text-center" id="buttongroup">
            <a class="btn btn-block btn-lg btn-success">12.491<br>Stellenangebote</a>
            <button class="btn btn-block btn-lg btn-success" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">1.443
              <br>Unternehmen</button>
            <div class="collapse" id="collapseExample">
              <div class="card card-block">Möchten Sie ebenfalls ein Teil der erfolgreichsten Job-Community Deutschlands sein? Melden Sie sich noch heute an.</div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="section" id="section-stellenangebote">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <h1>Aktuelle Stellenangebote</h1>
          </div>
          <div id="filter-button" class="col-md-4" style="display:none;">
            <div class="btn-group btn-group-lg" data-toggle="buttons">
              <a class="btn btn-success dropdown-toggle" data-toggle="dropdown"> Filter <span class="fa fa-caret-down"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li>
                  <a href="#">Studiengang</a>
                </li>
                <li>
                  <a href="#">Ort</a>
                </li>
                <li>
                  <a href="#">Zeit</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <ul class="media-list">
              <li class="media">
                <img class="media-object" src="images\pwc-Logo.png" height="64" width="64">
                <div class="media-body">
                  <h4 class="media-heading">PWC</h4>
                  <p>Rechtsreferendar (w/m) Energierecht</p>
                </div>
              </li>
              <li class="media">
                <img class="media-object" src="images\NSK-Logo.png" height="64" width="64">
                <div class="media-body">
                  <h4 class="media-heading">NSK Deutschland GmbH</h4>
                  <p>Trainee (w/m) Electrical Engineering - Automotive Lenksysteme -</p>
                </div>
              </li>
              <li class="media">
                <img class="media-object" src="images\T-Kom-Logo.png" height="64" width="64">
                <div class="media-body">
                  <h4 class="media-heading">Deutsche Telekom AG</h4>
                  <p>Trainees (m/w) in der IT- und Fachberatung</p>
                </div>
              </li>
              <li class="media">
                <img class="media-object" src="images\T-Kom-Logo.png" height="64" width="64">
                <div class="media-body">
                  <h4 class="media-heading">Deutsche Telekom AG</h4>
                  <p>Dualer Master (m/w) im Bereich Human Resources</p>
                </div>
              </li>
            </ul>
            <div class="row">
              <div class="col-md-12">
                <a href="stellenangebote.php" class="btn btn-block btn-lg btn-success">Weitere Stellenangebote erkunden!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="modal fade" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Registrierung</h4>
          </div>
          <div class="modal-body">
            <form role="form" method="post" action="./register.php?page=2">
              <div class="form-group">
                <label class="control-label" for="exampleInputEmail1">Vorname</label>
                <input class="form-control" name="firstname" placeholder="Vorname" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="exampleInputPassword1">Nachname</label>
                <input class="form-control" name="lastname" placeholder="Nachname" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="exampleInputEmail1">E-Mail-Address</label>
                <input class="form-control" name="email" id="exampleInputEmail1" placeholder="E-Mail-Adresse" type="email" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="exampleInputPassword1">Paswort</label>
                <input class="form-control" name="password" id="password" placeholder="Passwort" type="password" >
              </div>
              <div class="form-group">
                <label class="control-label" for="exampleInputPassword1">Passwort wiederholen</label>
                <input class="form-control" name="password_confirm" id="password_confirm" placeholder="Passwort wiederholen" type="password"  onkeyup="checkPass(); return false;">
              </div>
              <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-primary active">
                  <input type="radio" name="options_graduate" id="option1" autocomplete="off"> Sind Sie Absolvent?</label>
                <label class="btn btn-primary">
                  <input type="radio" name="options_company" id="option2" autocomplete="off">Repräsentieren Sie ein Unternehmen?</label>
              </div>
              <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal">Schließen</a>
                <button type="submit" name="submit" class="btn btn-success">Los geht's!</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <footer class="section">
      <div class="container">
        <div class="row">
          <div class="col-sm-3 text-center">
            <img src="images\logo_green_LightBG.png" id="footer-logo" class="img-responsive">
            <div class="row">
              <div class="col-md-12 text-center">
                <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor
                  incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                  nostrud</p>
              </div>
            </div>
          </div>
          <div class="col-sm-2 text-center">
            <p class="text-info text-right"></p>
            <a href="impressum.php"><h3>Informationen</h3></a>
            <div class="row">
              <div class="col-md-12 text-center">
                <p class="text-center">Über uns
                  <br>Presse
                  <br>Arbeitgeber
                  <br>Karriere
                  <br>Kontakt</p>
              </div>
            </div>
          </div>
          <div class="col-sm-2">
            <p class="text-info text-right"></p>
            <a href="impressum.php"><h3 class="text-center">Webseite</h3></a>
            <div class="row">
              <div class="col-md-12 text-center">
                <p class="text-center">Sitemap
                  <br>Mobil
                  <br>Datenschutzerklärung
                  <br>Nutzungsbedingungen
                  <br>AGB</p>
              </div>
            </div>
          </div>
          <div class="col-sm-2">
            <p class="text-info text-right"></p>
            <a href="impressum.php"><h3 class="text-center">Unternehmen</h3></a>
            <div class="row">
              <div class="col-md-12 text-center">
                <p class="text-center">Team
                  <br>Location
                  <br>Karte
                  <br>History</p>
              </div>
            </div>
          </div>
          <div class="col-sm-2">
            <p class="text-info text-right"></p>
            <h3 class="text-center">Social</h3>
            <p></p>
            <div class="row">
              <div class="col-md-12 text-center">
                <a href="#"><i class="fa fa-3x fa-fw fa-instagram"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-twitter"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-facebook"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-github"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>

</body></html>