<?php

// Credentials
$dbhost = "localhost";
$dbname = "graduatemarket";
$dbuser = "root";
$dbpass = "";

//	Connection
global $db;

$db = new mysqli();
$db->connect($dbhost, $dbuser, $dbpass, $dbname);
$db->set_charset("utf8");

//	Check Connection
if ($db->connect_errno) {
    printf("Verbindung fehlgeschlagen: %s\n", $db->connect_error);
    exit();
}


// Define Output HTML Formating
$output = '';
$output .= '<li class="media well results" >';
$output .= '<a class="pull-left" href="idString"><img class="media-object" src="images\placeholder-portrait.png" height="64" width="64"></a>';
$output .= '<div class="media-body">';
$output .= '<h4 class="media-heading"> nameString </h4>';
$output .= '<p> Fähigkeiten: skillsString <br> Studiengang: courseString</p>';
$output .= '<a class="btn btn-success" href="idString">Mehr Informationen</a>';
$output .= '</div>';
$output .= '</li>';

// Get Search
$search_string = preg_replace("/[^A-Za-z0-9]/", " ", $_POST['query']);
$search_string = $db->real_escape_string($search_string);

// Check Length Must More Than One Character
if (strlen($search_string) >= 1 && $search_string !== ' ') {
    // Build Query
    $query = 'SELECT * FROM user WHERE course LIKE "%'.$search_string.'%" OR firstname LIKE "%'.$search_string.'%" OR skills LIKE "%'.$search_string.'%"';

    // Do Search
    $result = $db->query($query);
    while($results = $result->fetch_array()) {
        $result_array[] = $results;
    }

    // Check If We Have Results
    if (isset($result_array)) {
        foreach ($result_array as $result) {

            // Format Output Strings And Hightlight Matches
            $display_name = preg_replace("/".$search_string."/i", "<b'>".$search_string."</b>", $result['firstname']);
            $display_skills = preg_replace("/".$search_string."/i", "<b>".$search_string."</b>", $result['skills']);
			$display_course = preg_replace("/".$search_string."/i", "<b>".$search_string."</b>", $result['course']);
            $display_id = 'show-profile.php?user='.urlencode($result['email']);

            // Insert Description
            $output = str_replace('skillsString', $display_skills, $output);

            // Insert Job Title
            $output = str_replace('nameString', $display_name, $output);

            // Insert URL
            $output = str_replace('idString', $display_id, $output);
			
			// Insert Course
            $output = str_replace('courseString', $display_course, $output);

            // Output
            echo($output);
        }
    }else{

        // Format No Results Output
        //$output = str_replace('idString', 'javascript:void(0);', $html);
        //$output = str_replace('nameString', '<b>No Results Found.</b>', $output);
        $output = str_replace('nameString', 'Leider keine Treffer', $output);
        $output = str_replace('idString', 'javascript:void(0)' , $output);
        

        // Output
        echo($output);
    }
}


/*
// Build Function List (Insert All Functions Into DB - From PHP)

// Compile Functions Array
$functions = get_defined_functions();
$functions = $functions['internal'];

// Loop, Format and Insert
foreach ($functions as $function) {
	$function_name = str_replace("_", " ", $function);
	$function_name = ucwords($function_name);

	$query = '';
	$query = 'INSERT INTO search SET id = "", function = "'.$function.'", name = "'.$function_name.'"';

	$db->query($query);
}
*/
?>