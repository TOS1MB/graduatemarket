<?php

//Database Connection
$connection = mysqli_connect('localhost', 'root', '', 'graduatemarket')
or die ("Connection Error");

$email =  $_REQUEST['user'];
//Data

//firstname
$firstname_query = "SELECT firstname FROM user WHERE email = '$email'";
$firstname_result = mysqli_query($connection, $firstname_query);
$firstname_fetch = mysqli_fetch_assoc($firstname_result);
$firstname = $firstname_fetch['firstname'];

//lastname
$lastname_query = "SELECT lastname FROM user WHERE email = '$email'";
$lastname_result = mysqli_query($connection, $lastname_query);
$lastname_fetch = mysqli_fetch_assoc($lastname_result);
$lastname = $lastname_fetch['lastname'];

//location
$location_query = "SELECT location FROM user WHERE email = '$email'";
$location_result = mysqli_query($connection, $location_query);
$location_fetch = mysqli_fetch_assoc($location_result);
$location = $location_fetch['location'];

//course
$course_query = "SELECT course FROM user WHERE email = '$email'";
$course_result = mysqli_query($connection, $course_query);
$course_fetch = mysqli_fetch_assoc($course_result);
$course = $course_fetch['course'];

//email
$email_query = "SELECT email FROM user WHERE email = '$email'";
$email_result = mysqli_query($connection, $email_query);
$email_fetch = mysqli_fetch_assoc($email_result);
$email = $email_fetch['email'];

//skills
$skills_query = "SELECT skills FROM user WHERE email = '$email'";
$skills_result = mysqli_query($connection, $skills_query);
$skills_fetch = mysqli_fetch_assoc($skills_result);
$skills = $skills_fetch['skills'];

?>

<html><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css\stylesheet.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="js/checkPassword.js"></script>
	<link rel="icon" type="image/ico" href="favicon.ico">
</head><body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="index.php" class="navbar-brand"><img height="20" alt="Brand" src="images\logo_green_LightBG.png"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="underline">
                    <a href="stellenangebote.php">JOB suchen</a>
                </li>
                <li class="underline">
                    <a href="unternehmen.php">Unternehmen finden</a>
                </li>
                <!--Checking if logged in. When yes display name else login form.-->
                <?php
                // Checking if you're logged in. When not you will have a nav login form to login or register.
                if (empty($_COOKIE['user'])){
                    ?>
                    <li class="dropdown" >
                        <a href = "#" class="dropdown-toggle underline" data-toggle = "dropdown" role = "button" aria-haspopup = "true" aria-expanded = "true" > Einloggen <span class="caret" ></span ></a >
                        <ul class="dropdown-menu" >
                            <li >
                                <a data-toggle = "modal" data-target = "#myModal" class="btn" > Registrieren</a >
                            </li >
                            <li role = "separator" class="divider" ></li >
                            <li class="dropdown-header" > Anmelden</li >
                            <li >
                                <div class="col-md-12" >
                                    <form role = "form" method = "post" action = "login.php?page=log" >
                                        <div class="form-group" >
                                            <label class="control-label" for="exampleInputEmail1" > Email - Adresse</label >
                                            <input class="form-control" id = "exampleInputEmail1" placeholder = "Enter email" type = "email" name = "email" >
                                        </div >
                                        <div class="form-group" >
                                            <label class="control-label" for="exampleInputPassword1" > Passwort</label >
                                            <input class="form-control" id = "exampleInputPassword1" placeholder = "Password" type = "password" name = "password" >
                                        </div >
                                        <button type = "submit" class="btn btn-default" > Anmelden!</button >
                                    </form >
                                </div >
                            </li >
                        </ul >
                    </li >
                <?php }
                //When you're logged in. The Login Form changes to a nav 'Your Profile' and a Logout Button gets added.
                else {
                    ?>
                    <li>
                        <a href = "profile-site.php" class="underline" role = "button" aria-haspopup = "true" aria-expanded = "true" > Your Profile </a >
                    </li>
                    <li>
                        <a href="logout.php" class="underline" role = "button" aria-haspopup = "true" aria-expanded = "true">Ausloggen</a>
                    </li>

                    <?php
                }
                ?>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <img src="images\placeholder-portrait.png" class="center-block img-circle img-responsive">
            </div>
            <div class="col-md-9">
                <h1><?php echo $firstname; ?></h1>
                <h3>Deine persönlichen Daten</h3>
                <ul class="list-group">
                    <?php if ($firstname) { ?>
                        <li class="list-group-item">
                            <label class="col-md-3">Vorname</label><?php echo $firstname; ?></li> <?php } else { echo "<li class=\"list-group-item\"><label class=\"col-md-3\">Vorname</label> - </li>";} ?>
                    <?php if ($lastname) { ?>
                        <li class="list-group-item">
                            <label class="col-md-3">Nachname</label><?php echo $lastname; ?></li> <?php } else { echo "<li class=\"list-group-item\"><label class=\"col-md-3\">Nachname</label> - </li>";} ?>
                    <?php if ($location) { ?>
                        <li class="list-group-item">
                            <label class="col-md-3">Wohnort</label><?php echo $location; ?></li> <?php } else { echo "<li class=\"list-group-item\"><label class=\"col-md-3\">Wohnort</label> - </li>";} ?>
                    <?php if ($course) { ?>
                        <li class="list-group-item">
                            <label class="col-md-3">Studiengang</label><?php echo $course; ?></li> <?php } else { echo "<li class=\"list-group-item\"><label class=\"col-md-3\">Studiengang</label> - </li>";} ?>
                    <?php if ($email) { ?>
                        <li class="list-group-item">
                            <label class="col-md-3">E-Mail-Adresse</label><?php echo $email; ?></li> <?php } else { echo "<li class=\"list-group-item\"><label class=\"col-md-3\">E-Mail-Adresse</label> - </li>";} ?>
                    <?php if ($skills) { ?>
                        <li class="list-group-item">
                            <label class="col-md-3">Skills</label><?php echo $skills; ?></li> <?php } else { echo "<li class=\"list-group-item\"><label class=\"col-md-3\">Skills</label> - </li>";} ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<section class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Registrierung</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="./register.php?page=2">
                    <div class="form-group">
                        <label class="control-label" for="exampleInputEmail1">Vorname</label>
                        <input class="form-control" name="firstname" placeholder="Vorname" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="exampleInputPassword1">Nachname</label>
                        <input class="form-control" name="lastname" placeholder="Nachname" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="exampleInputEmail1">E-Mail-Address</label>
                        <input class="form-control" name="email" id="exampleInputEmail1" placeholder="E-Mail-Adresse" type="email" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="exampleInputPassword1">Paswort</label>
                        <input class="form-control" name="password" id="password" placeholder="Passwort" type="password" >
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="exampleInputPassword1">Passwort wiederholen</label>
                        <input class="form-control" name="password_confirm" id="password_confirm" placeholder="Passwort wiederholen" type="password"  onkeyup="checkPass(); return false;">
                    </div>
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-primary active">
                            <input type="radio" name="options_graduate" id="option1" autocomplete="off"> Sind Sie Absolvent?</label>
                        <label class="btn btn-primary">
                            <input type="radio" name="options_company" id="option2" autocomplete="off">Repräsentieren Sie ein Unternehmen?</label>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-default" data-dismiss="modal">Schließen</a>
                        <button type="submit" name="submit" class="btn btn-success">Los geht's!</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<footer class="section">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 text-center">
                <img src="images\logo_green_LightBG.png" id="footer-logo" class="img-responsive">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor
                            incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                            nostrud</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-2 text-center">
                <p class="text-info text-right"></p>
                <a href="impressum.php"><h3>Informationen</h3></a>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p class="text-center">Über uns
                            <br>Presse
                            <br>Arbeitgeber
                            <br>Karriere
                            <br>Kontakt</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <p class="text-info text-right"></p>
                <a href="impressum.php"><h3 class="text-center">Webseite</h3></a>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p class="text-center">Sitemap
                            <br>Mobil
                            <br>Datenschutzerklärung
                            <br>Nutzungsbedingungen
                            <br>AGB</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <p class="text-info text-right"></p>
                <a href="impressum.php"><h3 class="text-center">Unternehmen</h3></a>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p class="text-center">Team
                            <br>Location
                            <br>Karte
                            <br>History</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <p class="text-info text-right"></p>
                <h3 class="text-center">Social</h3>
                <p></p>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="#"><i class="fa fa-3x fa-fw fa-instagram"></i></a>
                        <a href="#"><i class="fa fa-3x fa-fw fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-3x fa-fw fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-3x fa-fw fa-github"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>


</body></html>