// Start Ready
$(document).ready(function() {

    // On Search Submit and Get Results
    function search() {
        var query_value = $('input#input-yp').val();
        //$('ul#results').text(query_value);
        if(query_value !== ''){
            $.ajax({
                type: "POST",
                url: "profile-search.php",
                data: { query: query_value },
                cache: false,
                success: function(html){
                    $("ul#results-profile").html(html);
                }
            });
        }return false;
    }

    $("input#input-yp").keyup(function(e) {
        // Set Timeout
        clearTimeout($.data(this, 'timer'));

        // Set Search String
        var search_string = $(this).val();

        // Do Search
        if (search_string == '') {
            $("ul#results-profile").fadeOut();
            $('h4#results-text').fadeOut();
        }else{
            $("ul#results-profile").fadeIn();
            $('h4#results-text').fadeIn();
            $(this).data('timer', setTimeout(search, 100));
        };
    });

});
