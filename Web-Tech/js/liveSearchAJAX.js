// Start Ready
$(document).ready(function() {

    // Icon Click Focus
    /*$('div.icon').click(function(){
     $('input#input-stellen-job').focus();
     });*/


    // On Search Submit and Get Results
    function search() {
        var query_value = $('input#input-stellen-job').val();
        //$('ul#results').text(query_value);
        if(query_value !== ''){
            $.ajax({
                type: "POST",
                url: "live-search.php",
                data: { query: query_value },
                cache: false,
                success: function(html){
                    $("ul#results").html(html);
                }
            });
        }return false;
    }

    $("input#input-stellen-job").keyup(function(e) {
        // Set Timeout
        clearTimeout($.data(this, 'timer'));

        // Set Search String
        var search_string = $(this).val();

        // Do Search
        if (search_string == '') {
            $("ul#results").fadeOut();
            $('h4#results-text').fadeOut();
        }else{
            $("ul#results").fadeIn();
            $('h4#results-text').fadeIn();
            $(this).data('timer', setTimeout(search, 100));
        };
    });

});
