<?php
if (isset($_COOKIE["user"])){
    unset($_COOKIE["user"]);
    setcookie("user", "", time() - (86400 * 30), '/');
}

if (isset($_COOKIE["id"])){
    unset($_COOKIE["id"]);
    setcookie("id", "", time() - (86400 * 30), '/');
}

?>
<html>
<head>
    <title>Logout</title>
		<link rel="icon" type="image/ico" href="favicon.ico">
        <meta http-equiv="refresh" content="2; index.php">
</head>
<body>
<div style="padding: 10px 35px 10px 14px; background-color: #ddf0d7; border-color:#a4cd98; color: #1c65c; border-radius: 4px;">
        Du hast dich nun ausgeloggt..
    </div>

</body>
</html>
