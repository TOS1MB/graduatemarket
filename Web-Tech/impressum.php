<?php
if (!empty($_COOKIE['user'])) {
  $logged_user = $_COOKIE['user'];

  $id = $_COOKIE['id'];
}
?>
<html><head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
  <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="css\stylesheet.css" rel="stylesheet" type="text/css">
  <link href="css\jquery.flexdatalist.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="js/jquery.flexdatalist.js"></script>
  <script type="text/javascript" src="js/checkPassword.js"></script>
  <link rel="icon" type="image/ico" href="favicon.ico">

</head><body>
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="index.php" class="navbar-brand"><img height="20" alt="Brand" src="images\logo_green_LightBG.png"></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        <li class="underline">
          <a href="stellenangebote.php">JOB suchen</a>
        </li>
        <li class="underline">
          <a href="unternehmen.php">Unternehmen finden</a>
        </li>

        <!--Checking if logged in. When yes display name else login form.-->
        <?php
        // Checking if you're logged in. When not you will have a nav login form to login or register.
        if (empty($_COOKIE['user'])){
          ?>
          <li class="dropdown" >
            <a href = "#" class="dropdown-toggle underline" data-toggle = "dropdown" role = "button" aria-haspopup = "true" aria-expanded = "true" > Einloggen <span class="caret" ></span ></a >
            <ul class="dropdown-menu" >
              <li >
                <a data-toggle = "modal" data-target = "#myModal" class="btn" > Registrieren</a >
              </li >
              <li role = "separator" class="divider" ></li >
              <li class="dropdown-header" > Anmelden</li >
              <li >
                <div class="col-md-12" >
                  <form role = "form" method = "post" action = "login.php?page=log" >
                    <div class="form-group" >
                      <label class="control-label" for="exampleInputEmail1" > Email - Adresse</label >
                      <input class="form-control" id = "exampleInputEmail1" placeholder = "Enter email" type = "email" name = "email" >
                    </div >
                    <div class="form-group" >
                      <label class="control-label" for="exampleInputPassword1" > Passwort</label >
                      <input class="form-control" id = "exampleInputPassword1" placeholder = "Password" type = "password" name = "password" >
                    </div >
                    <button type = "submit" class="btn btn-default" > Anmelden!</button >
                  </form >
                </div >
              </li >
            </ul >
          </li >
        <?php }
        //When you're logged in. The Login Form changes to a nav 'Your Profile' and a Logout Button gets added.
        else {
          ?>
          <li>
            <a href = "profile-site.php" class="underline" role = "button" aria-haspopup = "true" aria-expanded = "true" > Your Profile </a >
          </li>
          <li>
            <a href="logout.php">Ausloggen</a>
          </li>

          <?php
        }
        ?>
      </ul>
    </div>
    <!--/.nav-collapse -->
  </div>
</nav>

<section class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 class="text-center">Impressum, AGB, Nutzungsbedinungen</h1>
          </div>
        </div>
        <div class="row">
          <div class="col-md-8">
            <h1>Impressum</h1>
            <a href="http://www.deutsche-anwaltshotline.de/recht-auf-ihrer-website/impressum-generator" rel="nofollow">Impressum dieser Website erstellt</a>über den Generator der Deutschen Anwaltshotline AG
            <br>
            <br>Angaben gem. § 5 TMG
            <br>
            <br>
            <b>Betreiber und Kontakt:</b>
            <br>GraduateMarket
            <br>
            <br>Musterstraße 1
            <br>78462 Konstanz
            <br>
            <br>Telefonnummer: 07531/123456
            <br>E-Mail-Adresse: muster.email@muster.de
            <br>
            <br>
            <b>Vertretung:</b>
            <br>GraduateMarket wird vertreten durch Max Mustermann
            <br>
            <br>
            <b>Verantwortlicher für journalistisch-redaktionelle Inhalte gem. § 55 II
              RstV:</b>
            <br>GraduateMarket-Redaktion
            <br>
            <br>
            <b>Bilder und Grafiken:</b>
            <br>Angaben der Quelle für verwendetes Bilder- und Grafikmaterial:
            <br>Aus dem Internet</div>
          <div class="col-md-4">
            <h1>Hier finden Sie uns</h1>
            <img class="center-block img-responsive img-rounded" alt="Unser Standort" src="http://maps.googleapis.com/maps/api/staticmap?center=Constance,+Germany&amp;zoom=13&amp;scale=2&amp;size=350x350&amp;maptype=hybrid&amp;format=png&amp;visual_refresh=true&amp;markers=size:mid%7Ccolor:0xff0000%7Clabel:%7CConstance,+Germany">
          </div>
        </div>
      </div>
    </section>
    <section class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 class="text-center">Das GraduateMarket Team</h1>
            <p class="text-center">Wir sind eine Gruppe begabter Individuen, die selbst erst vor kurzen ihr
              Studium erfolgreich abgeschlossen haben
              <br>&nbsp;und die Hürde gut kennen, welche auf einen als Hochschulabsolvent
              zukommen</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <img src="images\placeholder-portrait.png" class="center-block img-circle img-responsive">
            <h3 class="text-center">Maximilian Guske</h3>
            <p class="text-center">Developer</p>
          </div>
          <div class="col-md-4">
            <img src="images\placeholder-portrait.png" class="center-block img-circle img-responsive">
            <h3 class="text-center">Tolga Sirvan</h3>
            <p class="text-center">Developer</p>
          </div>
          <div class="col-md-4">
            <img src="images\placeholder-portrait.png" class="center-block img-circle img-responsive">
            <h3 class="text-center">Wikipedia</h3>
            <p class="text-center">Redaktion</p>
          </div>
        </div>
      </div>
    </section>
    <section class="modal fade" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Registrierung</h4>
          </div>
          <div class="modal-body">
            <form role="form" method="post" action="./register.php?page=2">
              <div class="form-group">
                <label class="control-label" for="exampleInputEmail1">Vorname</label>
                <input class="form-control" name="firstname" placeholder="Vorname" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="exampleInputPassword1">Nachname</label>
                <input class="form-control" name="lastname" placeholder="Nachname" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="exampleInputEmail1">E-Mail-Address</label>
                <input class="form-control" name="email" id="exampleInputEmail1" placeholder="E-Mail-Adresse" type="email" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="exampleInputPassword1">Paswort</label>
                <input class="form-control" name="password" id="password" placeholder="Passwort" type="password" >
              </div>
              <div class="form-group">
                <label class="control-label" for="exampleInputPassword1">Passwort wiederholen</label>
                <input class="form-control" name="password_confirm" id="password_confirm" placeholder="Passwort wiederholen" type="password"  onkeyup="checkPass(); return false;">
              </div>
              <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-primary active">
                  <input type="radio" name="options_graduate" id="option1" autocomplete="off"> Sind Sie Absolvent?</label>
                <label class="btn btn-primary">
                  <input type="radio" name="options_company" id="option2" autocomplete="off">Repräsentieren Sie ein Unternehmen?</label>
              </div>
              <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal">Schließen</a>
                <button type="submit" name="submit" class="btn btn-success">Los geht's!</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
<footer class="section">
      <div class="container">
        <div class="row">
          <div class="col-sm-3 text-center">
            <img src="images\logo_green_LightBG.png" id="footer-logo" class="img-responsive">
            <div class="row">
              <div class="col-md-12 text-center">
                <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor
                  incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                  nostrud</p>
              </div>
            </div>
          </div>
          <div class="col-sm-2 text-center">
            <p class="text-info text-right"></p>
            <a href="impressum.php"><h3>Informationen</h3></a>
            <div class="row">
              <div class="col-md-12 text-center">
                <p class="text-center">Über uns
                  <br>Presse
                  <br>Arbeitgeber
                  <br>Karriere
                  <br>Kontakt</p>
              </div>
            </div>
          </div>
          <div class="col-sm-2">
            <p class="text-info text-right"></p>
            <a href="impressum.php"><h3 class="text-center">Webseite</h3></a>
            <div class="row">
              <div class="col-md-12 text-center">
                <p class="text-center">Sitemap
                  <br>Mobil
                  <br>Datenschutzerklärung
                  <br>Nutzungsbedingungen
                  <br>AGB</p>
              </div>
            </div>
          </div>
          <div class="col-sm-2">
            <p class="text-info text-right"></p>
            <a href="impressum.php"><h3 class="text-center">Unternehmen</h3></a>
            <div class="row">
              <div class="col-md-12 text-center">
                <p class="text-center">Team
                  <br>Location
                  <br>Karte
                  <br>History</p>
              </div>
            </div>
          </div>
          <div class="col-sm-2">
            <p class="text-info text-right"></p>
            <h3 class="text-center">Social</h3>
            <p></p>
            <div class="row">
              <div class="col-md-12 text-center">
                <a href="#"><i class="fa fa-3x fa-fw fa-instagram"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-twitter"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-facebook"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-github"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  

</body></html>