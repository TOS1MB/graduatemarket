Web-Technologie-Projekt SS16 - statische Webseite

1. Meilenstein:
Webseiten sind nur teilweise animiert durch JavaScript. Bilder wiederholen sich aufgrund von lizenztechnischen Gr�nden.
Buttons, die eigentlich auf externe Seiten f�hren, haben keine Weiterleitung, da diese auf Webseiten von Unternehmen f�hren w�rden.
Das Impressum wurde mit einem Generator erzeugt. Hierf�r wurden fiktive Daten verwendet.

2. Meilenstein:
Eine statische Webseite, die haupts�chlich mit PHP, JavaScript und AJAX dynamisiert wurde. Augenmerk wurde auf das implmentieren eines 
Funktionierenden Login gesetzt. Dadurch ist ein verlinken auf andere Profile und Stellenangebote m�glich.

Installationshinweis:
F�r den Start unserer Web-Applikation ist lediglich ein Server und eine Datenbankanbindung n�tig. Hier empfiehlt sich Xampp, da �ber
localhost/phpmyadmin leicht eine SQL-Datei importiert werden kann. Daf�r muss eine DB mit dem Namen "graduatemarket" erstellt werden und
das SQL-Skript importiert werden (Reiter in der Men�-Leiste). Danach kann das Projekt �ber "localhost/web-tech/index.php" aufgerufen werden.

3. und letzter Meilenstein:
Unter der "deployment instruction" findet man Fakten und Installationshinweise zur Applikation. Hier erf�hrt man ebenfalls alle wichtigen 
Informationen, wie man die einzelnen Konfigurationen druchf�hrt.


Bearbeiter:
Maximilian Guske
Tolga Sirvan