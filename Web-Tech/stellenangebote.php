<?php
if (!empty($_COOKIE['user'])) {
  $logged_user = $_COOKIE['user'];

  $id = $_COOKIE['id'];
}
?>
<html><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css\stylesheet.css" rel="stylesheet" type="text/css">
    <link href="css\jquery.flexdatalist.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="js/jquery.flexdatalist.js"></script>
    <script type="text/javascript" src="js/liveSearchAJAX.js"></script>
    <script type="text/javascript" src="js/checkPassword.js"></script>
	<link rel="icon" type="image/ico" href="favicon.ico">
  </head><body>
    <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="index.php" class="navbar-brand"><img height="20" alt="Brand" src="images\logo_green_LightBG.png"></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        <li class="underline">
          <a href="stellenangebote.php">JOB suchen</a>
        </li>
        <li class="underline">
          <a href="unternehmen.php">Unternehmen finden</a>
        </li>

        <!--Checking if logged in. When yes display name else login form.-->
        <?php
        // Checking if you're logged in. When not you will have a nav login form to login or register.
        if (empty($_COOKIE['user'])){
          ?>
          <li class="dropdown" >
            <a href = "#" class="dropdown-toggle underline" data-toggle = "dropdown" role = "button" aria-haspopup = "true" aria-expanded = "true" > Einloggen <span class="caret" ></span ></a >
            <ul class="dropdown-menu" >
              <li >
                <a data-toggle = "modal" data-target = "#myModal" class="btn" > Registrieren</a >
              </li >
              <li role = "separator" class="divider" ></li >
              <li class="dropdown-header" > Anmelden</li >
              <li >
                <div class="col-md-12" >
                  <form role = "form" method = "post" action = "login.php?page=log" >
                    <div class="form-group" >
                      <label class="control-label" for="exampleInputEmail1" > Email - Adresse</label >
                      <input class="form-control" id = "exampleInputEmail1" placeholder = "Enter email" type = "email" name = "email" >
                    </div >
                    <div class="form-group" >
                      <label class="control-label" for="exampleInputPassword1" > Passwort</label >
                      <input class="form-control" id = "exampleInputPassword1" placeholder = "Password" type = "password" name = "password" >
                    </div >
                    <button type = "submit" class="btn btn-default" > Anmelden!</button >
                  </form >
                </div >
              </li >
            </ul >
          </li >
        <?php }
        //When you're logged in. The Login Form changes to a nav 'Your Profile' and a Logout Button gets added.
        else {
          ?>
          <li>
            <a href = "profile-site.php" class="underline" role = "button" aria-haspopup = "true" aria-expanded = "true" > Your Profile </a >
          </li>
          <li>
            <a href="logout.php" class="underline" role = "button" aria-haspopup = "true" aria-expanded = "true">Ausloggen</a>
          </li>

          <?php
        }
        ?>
      </ul>
    </div>
    <!--/.nav-collapse -->
  </div>
</nav>
    <section class="cover">
      <div class="background-image-fixed cover-image" style="background-image : url('images/stellenangebote_overlay.png')"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center" id="head-well">
            <h1 class="text-inverse">Stellenangebote!</h1>
            <p class="text-inverse">Du musst dir nicht mehr den Kopf zerbrechen, wo du die besten Jobangebote
              für dich finden kannst.
              <br>Hier findest du auf deine Bedürfnisse abgestimmte Stellenangebote, die
              sich alle
              <br>an Hochschulabsolventen richten!</p>
          </div>
        </div>
      </div>
    </section>
    <section class="section" id="premium-stellen-block">
      <div class="container">
        <div class="row">
          <div class="col-md-12 " id="mittelblock-stellen">
            <div class="section">
              <div class="container">
                <div class="row">
                  <div class="col-md-12">
                    <h1 class="text-center">Premiun-Stellenangebote</h1>
                    <p class="text-center">Exklusive Jobs für dich!</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-2">
                    <img src="images\logo-BMW.png" class="img-circle img-responsive">
                  </div>
                  <div class="col-md-4">
                    <h3 class="text-left">BMW</h3>
                    <p class="text-left">Lorem ipsum dolor sit amet, adipiscing elit Aenean commodo ligula eget.</p>
                  </div>
                  <div class="col-md-2">
                    <img src="images\logo-BMW.png" class="img-circle img-responsive">
                  </div>
                  <div class="col-md-4">
                    <h3 class="text-left">BMW</h3>
                    <p class="text-left">Lorem ipsum dolor sit amet, adipiscing elit Aenean commodo ligula eget.</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-2">
                    <img src="images\logo-BMW.png" class="img-circle img-responsive">
                  </div>
                  <div class="col-md-4">
                    <h3 class="text-left">BMW</h3>
                    <p class="text-left">Lorem ipsum dolor sit amet, adipiscing elit Aenean commodo ligula eget.</p>
                  </div>
                  <div class="col-md-2">
                    <img src="images\logo-BMW.png" class="img-circle img-responsive">
                  </div>
                  <div class="col-md-4 text-center">
                    <h3 class="text-left">BMW</h3>
                    <p class="text-left">Lorem ipsum dolor sit amet, adipiscing elit Aenean commodo ligula eget.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="section" id="section-stellenangebote">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1>Alle Stellenangebote</h1>
          </div>
        </div>
        <div class="row">
          <div class="section dunkelblau">
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <form class="form-horizontal" role="form">
                    <div class="form-group">
                      <div class="col-sm-2"></div>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" autocomplete="off" spellcheck="false" id="input-stellen-job" placeholder="Job oder Position">
                      </div>
                      <div class="col-sm-2"></div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <ul class="media-list" id="results">
              <li class="media well">
                <a class="pull-left" href="#"><img class="media-object" src="images\pwc-Logo.png" height="64" width="64"></a>
                <div class="media-body">
                  <h4 class="media-heading">PWC</h4>
                  <p>Rechtsreferendar (w/m) Energierecht</p>
                  <a class="btn btn-success">Mehr Informationen</a>
                </div>
              </li>
              <li class="media well">
                <a class="pull-left" href="#"><img class="media-object" src="images\pwc-Logo.png" height="64" width="64"></a>
                <div class="media-body">
                  <h4 class="media-heading">PWC</h4>
                  <p>Rechtsreferendar (w/m) Energierecht</p>
                  <a class="btn btn-success">Mehr Informationen</a>
                </div>
              </li>
              <li class="media well">
                <a class="pull-left" href="#"><img class="media-object" src="images\pwc-Logo.png" height="64" width="64"></a>
                <div class="media-body">
                  <h4 class="media-heading">PWC</h4>
                  <p>Rechtsreferendar (w/m) Energierecht</p>
                  <a class="btn btn-success">Mehr Informationen</a>
                </div>
              </li>
              <li class="media well">
                <a class="pull-left" href="#"><img class="media-object" src="images\NSK-Logo.png" height="64" width="64"></a>
                <div class="media-body">
                  <h4 class="media-heading">NSK Deutschland GmbH</h4>
                  <p>Trainee (w/m) Electrical Engineering - Automotive Lenksysteme -</p>
                  <a class="btn btn-success">Mehr Informationen</a>
                </div>
              </li>
              <li class="media well">
                <a class="pull-left" href="#"><img class="media-object" src="images\NSK-Logo.png" height="64" width="64"></a>
                <div class="media-body">
                  <h4 class="media-heading">NSK Deutschland GmbH</h4>
                  <p>Trainee (w/m) Electrical Engineering - Automotive Lenksysteme -</p>
                  <a class="btn btn-success">Mehr Informationen</a>
                </div>
              </li>
              <li class="media well">
                <a class="pull-left" href="#"><img class="media-object" src="images\NSK-Logo.png" height="64" width="64"></a>
                <div class="media-body">
                  <h4 class="media-heading">NSK Deutschland GmbH</h4>
                  <p>Trainee (w/m) Electrical Engineering - Automotive Lenksysteme -</p>
                  <a class="btn btn-success">Mehr Informationen</a>
                </div>
              </li>
              <li class="media well">
                <a class="pull-left" href="#"><img class="media-object" src="images\T-Kom-Logo.png" height="64" width="64"></a>
                <div class="media-body">
                  <h4 class="media-heading">Deutsche Telekom AG</h4>
                  <p>Trainees (m/w) in der IT- und Fachberatung</p>
                  <a class="btn btn-success">Mehr Informationen</a>
                </div>
              </li>
              <li class="media well">
                <a class="pull-left" href="#"><img class="media-object" src="images\T-Kom-Logo.png" height="64" width="64"></a>
                <div class="media-body">
                  <h4 class="media-heading">Deutsche Telekom AG</h4>
                  <p>Trainees (m/w) in der IT- und Fachberatung</p>
                  <a class="btn btn-success">Mehr Informationen</a>
                </div>
              </li>
              <li class="media well">
                <a class="pull-left" href="#"><img class="media-object" src="images\T-Kom-Logo.png" height="64" width="64"></a>
                <div class="media-body">
                  <h4 class="media-heading">Deutsche Telekom AG</h4>
                  <p>Trainees (m/w) in der IT- und Fachberatung</p>
                  <a class="btn btn-success">Mehr Informationen</a>
                </div>
              </li>
              <li class="media well">
                <a class="pull-left" href="#"><img class="media-object" src="images\T-Kom-Logo.png" height="64" width="64"></a>
                <div class="media-body">
                  <h4 class="media-heading">Deutsche Telekom AG</h4>
                  <p>Dualer Master (m/w) im Bereich Human Resources</p>
                  <a class="btn btn-success">Mehr Informationen</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <section class="modal fade" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Registrierung</h4>
          </div>
          <div class="modal-body">
            <form role="form" method="post" action="./register.php?page=2">
              <div class="form-group">
                <label class="control-label" for="exampleInputEmail1">Vorname</label>
                <input class="form-control" name="firstname" placeholder="Vorname" required="">
              </div>
              <div class="form-group">
                <label class="control-label" for="exampleInputPassword1">Nachname</label>
                <input class="form-control" name="lastname" placeholder="Nachname" required="">
              </div>
              <div class="form-group">
                <label class="control-label" for="exampleInputEmail1">E-Mail-Address</label>
                <input class="form-control" name="email" id="exampleInputEmail1" placeholder="E-Mail-Adresse" type="email" required="">
              </div>
              <div class="form-group">
                <label class="control-label" for="exampleInputPassword1">Paswort</label>
                <input class="form-control" name="password" id="password" placeholder="Passwort" type="password">
              </div>
              <div class="form-group">
                <label class="control-label" for="exampleInputPassword1">Passwort wiederholen</label>
                <input class="form-control" name="password_confirm" id="password_confirm" placeholder="Passwort wiederholen" type="password" onkeyup="checkPass(); return false;">
              </div>
              <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-primary active">
                  <input type="radio" name="options_graduate" id="option1" autocomplete="off">Sind Sie Absolvent?</label>
                <label class="btn btn-primary">
                  <input type="radio" name="options_company" id="option2" autocomplete="off">Repräsentieren Sie ein Unternehmen?</label>
              </div>
              <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal">Schließen</a>
                <button type="submit" name="submit" class="btn btn-success">Los geht's!</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <footer class="section">
      <div class="container">
        <div class="row">
          <div class="col-sm-3 text-center">
            <img src="images\logo_green_LightBG.png" id="footer-logo" class="img-responsive">
            <div class="row">
              <div class="col-md-12 text-center">
                <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor
                  incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                  nostrud</p>
              </div>
            </div>
          </div>
          <div class="col-sm-2 text-center">
            <p class="text-info text-right"></p>
            <a href="impressum.php"><h3>Informationen</h3></a>
            <div class="row">
              <div class="col-md-12 text-center">
                <p class="text-center">Über uns
                  <br>Presse
                  <br>Arbeitgeber
                  <br>Karriere
                  <br>Kontakt</p>
              </div>
            </div>
          </div>
          <div class="col-sm-2">
            <p class="text-info text-right"></p>
            <a href="impressum.php"><h3 class="text-center">Webseite</h3></a>
            <div class="row">
              <div class="col-md-12 text-center">
                <p class="text-center">Sitemap
                  <br>Mobil
                  <br>Datenschutzerklärung
                  <br>Nutzungsbedingungen
                  <br>AGB</p>
              </div>
            </div>
          </div>
          <div class="col-sm-2">
            <p class="text-info text-right"></p>
            <a href="impressum.php"><h3 class="text-center">Unternehmen</h3></a>
            <div class="row">
              <div class="col-md-12 text-center">
                <p class="text-center">Team
                  <br>Location
                  <br>Karte
                  <br>History</p>
              </div>
            </div>
          </div>
          <div class="col-sm-2">
            <p class="text-info text-right"></p>
            <h3 class="text-center">Social</h3>
            <p></p>
            <div class="row">
              <div class="col-md-12 text-center">
                <a href="#"><i class="fa fa-3x fa-fw fa-instagram"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-twitter"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-facebook"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-github"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  

</body></html>